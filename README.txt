This module gives greater control over the block visibility setting allowing you to do things like.

* Negate paths using a !

    Negate paths using a !
    You can easily say something like
    products/*
    !products/cheese/
    The block will show on every products/* page, except products/cheese/


* Use Regular Expressions
    This is pretty simple, this allows you to use any kind of regular expression you wish to match to your path. It only requires using a # to start end end the expression.
    Example
        products/*
        #catalog/[^/]+$#
        search/*

NOTE: You cannot combine both Regex and Negate.

You can get pretty complex with what you want to do. With regular expressions you can pretty much match anything you want.